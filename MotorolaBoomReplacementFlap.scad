/*
Replacement windscreen flap/on-switch for Motorola Boom Bluetooth headset.

Written by D. Scott Boggs in July of 2017, contact at scott@tams.tech.

I'm not sure of how well the GPL applies to hardware, but it should apply to this code at the very least. Basically, if you're going to make or redistribute this or a derivative of this, include the .scad file, and I won't complain at all. I would also prefer attribution, and I'm always looking to make contacts with common interests, so don't hestiate to contact me if you would like some help or collaboration.
*/

rounding_val	=	1;
width			=	11;
wedge_offset	=	2;
length			=	17;
thickness		=	3;
hinge_diameter	=	6;
hinge_radius	=	hinge_diameter/2;
hinge_depth		=	1.75;
hinge_hole_dia	=	4;
hinge_hole_dep	=	1.25;
hinge_extension	=	5;

module flap(){
	difference(){
		linear_extrude(height=3)
			offset ( r=rounding_val, $fs=1)
			polygon ( points=[[0+rounding_val,0+rounding_val],
				[wedge_offset+rounding_val,length-rounding_val],
				[width-wedge_offset-rounding_val,length-rounding_val],
				[width-rounding_val,0+rounding_val]] );
		translate ([ 1.1+rounding_val,rounding_val/2,1.6 ])
			scale ([ .6, .6, 1.5 ])
			linear_extrude ( height=1)
			offset ( r=rounding_val, $fs=1)
			polygon ( points=[[0,0], [2,20], [9,20], [11,0]] );
	}
}
module hinge(){
	difference(){
		union(){
			cylinder ( h=hinge_depth, d=hinge_diameter);
			translate ([ hinge_radius/2,0,hinge_depth ])
				rotate([-45,90,0])
				cube ([ hinge_depth*.85, hinge_radius,hinge_extension ]);
		}
		translate( [0,0,-0.01]) cylinder ( h=hinge_hole_dep, d=hinge_hole_dia);
	}
}
module otherHinge(){
	difference(){
		union(){
			cylinder ( h=hinge_depth, d=hinge_diameter, $fs=1);
			translate ([ 0,hinge_radius/2,hinge_depth ])
				rotate([180+45,90,0])
				cube ([ hinge_depth*.85, hinge_radius, hinge_extension]);
		}
		translate( [0,0,-0.01]) cylinder ( h=hinge_hole_dep, d=hinge_hole_dia, $fn=4);
	}
}
flap();
translate( [ width-hinge_depth-.2, -hinge_extension/2, thickness+hinge_extension/2 ]) rotate ([ 0,90,0 ]) hinge($fs=1);
translate( [ hinge_depth+.1, -hinge_extension/2,thickness+hinge_extension/2])rotate([ 0,-90,0 ]) otherHinge();